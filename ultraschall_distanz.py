#! /bin/env python

#Bibliotheken einbinden
import RPi.GPIO as GPIO
import time
 
#GPIO Modus (BOARD / BCM)
GPIO.setmode(GPIO.BCM)
 
#GPIO Pins zuweisen
GPIO_TRIGGER = 18
GPIO_ECHO = 24
GPIO_LED = 25
GPIO_BUZZER = 7

ALARMSTUFE1 = 10
ALARMSTUFE2 = 4

#Richtung der GPIO-Pins festlegen (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)
GPIO.setup(GPIO_LED, GPIO.OUT)
GPIO.setup(GPIO_BUZZER, GPIO.OUT)

def distanz():
    # setze Trigger auf HIGH
    GPIO.output(GPIO_TRIGGER, True)
 
    # setze Trigger nach 0.01ms aus LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)
 
    StartZeit = time.time()
    StopZeit = time.time()
 
    # speichere Startzeit
    while GPIO.input(GPIO_ECHO) == 0:
        StartZeit = time.time()
 
    # speichere Ankunftszeit
    while GPIO.input(GPIO_ECHO) == 1:
        StopZeit = time.time()
 
    # Zeit Differenz zwischen Start und Ankunft
    TimeElapsed = StopZeit - StartZeit
    # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
    # und durch 2 teilen, da hin und zurueck
    distanz = (TimeElapsed * 34300) / 2
 
    return distanz
 
if __name__ == '__main__':
    try:
        GPIO.output(GPIO_LED, True)
        GPIO.output(GPIO_BUZZER, True)
        time.sleep(0.5)
        GPIO.output(GPIO_LED, False)
        GPIO.output(GPIO_BUZZER, False)

        while True:
            abstand = distanz()
            print ("Gemessene Entfernung = %.1f cm" % abstand)
            if abstand > ALARMSTUFE2:
                GPIO.output(GPIO_BUZZER, False)
            if abstand > ALARMSTUFE1:
                GPIO.output(GPIO_LED, False)
            if abstand <= ALARMSTUFE1:
                GPIO.output(GPIO_LED, True)
            if abstand <= ALARMSTUFE2:
                GPIO.output(GPIO_BUZZER, True)
            time.sleep(1)

# Beim Abbruch durch STRG+C resetten
    except KeyboardInterrupt:
        print("Messung vom User gestoppt")
        GPIO.cleanup()

        
